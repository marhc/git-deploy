const commons = require("wikimedia-commons-file-path");

const getWiki = require("wikijs").default;

const apiUrl = "https://pt.wikipedia.org/w/api.php";

module.exports = {
  async getInfoBox(pageTitle) {
    const infoBox = await getWiki({
      apiUrl: apiUrl
    })
      .page(pageTitle)
      .then(page => page.info());
    return infoBox;
  },
  async getLinksOnly(pageTitle) {
    const links = await getWiki({
      apiUrl: apiUrl
    })
      .page(pageTitle)
      .then(page => page.links());
    return links;
  },
  async getLinks(pageTitle) {
    const links = await getWiki({
      apiUrl: apiUrl
    })
      .page(pageTitle)
      .then(async page => {
        return { html: await page.html(), titles: await page.links() };
      });
    return links;
  },
  async getHtmlPage(pageTitle) {
    const html = await getWiki({
      apiUrl: apiUrl
    })
      .page(pageTitle)
      .then(page => page.html());
    return html;
  },
  async getMainImage(pageTitle) {
    const image = await getWiki({
      apiUrl: apiUrl
    })
      .page(pageTitle)
      .then(page => page.mainImage());
    return image;
  },
  getImageUrl(image) {
    let result = image;
    if (Boolean(image)) {
      result = commons("File:" + image);
    }
    return result;
  }
};
