const Model = require("./models");

const { professionsFile, occupationsFile } = require("./dataset");

const { getTable } = require("./csvParser");

const { removeFile } = require("./util");

const { getPeopleFromWiki } = require("./scraping");

async function main() {
  const professions = getTable(professionsFile, Model.professions).filter(
    profession => profession.article_title != null
  );

  if (professions.length == 0) {
    console.log("No profession has an associated wiki title.");
  } else {
    removeFile(occupationsFile);

    for (const profession of professions) {
      const exclusions = profession.exclusions.split(",");
      await getPeopleFromWiki(
        profession.article_title,
        exclusions,
        profession.id
      );
    }
  }
}

main();
