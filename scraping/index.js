const $ = require("cheerio");

const Model = require("../models");

const baseUrl = "https://pt.wikipedia.org";

const { getInfoBox, getImageUrl, getLinks } = require("../wikiClient");

const { peopleFile, occupationsFile } = require("../dataset");

const { getTable, addRecord } = require("../csvParser");

const {
  filterWikiLinks,
  getAttribs,
  filterWebLinks,
  getUniqueLinks,
  pause
} = require("../util");

const updatePeopleTable = async (linkArray, professionId) => {
  const people = getTable(peopleFile, Model.people);

  for (const target of linkArray) {
    const linkTitle = target.href.replace("/wiki/", "");

    const peopleTitle = decodeURI(linkTitle.replace(/_/g, " "));

    const peopleUrl = baseUrl + target.href;

    const findPeople = people.filter(record => record.bio_url == peopleUrl);

    if (findPeople.length == 0) {
      const infoBox = await getInfoBox(peopleTitle);

      const newPeople = {
        link_title: linkTitle,
        name: peopleTitle,
        bio_url: peopleUrl,
        image_url: getImageUrl(infoBox.imagem)
      };

      console.log(
        "********************** NEW RECORD **********************\n",
        addRecord(newPeople, peopleFile)
      );
    } else {
      console.log(peopleTitle, "-", peopleUrl);
    }

    await pause(10);

    const newOccupation = {
      profession_id: professionId,
      link_title: linkTitle
    };
    addRecord(newOccupation, occupationsFile);
  }
};

module.exports = {
  async getPeopleFromWiki(wikiTitle, exclusions, professionId) {
    const wikiLinks = await getLinks(wikiTitle);

    const peopleList = filterWikiLinks(wikiLinks.titles, exclusions);

    const webLinks = getAttribs($("a", wikiLinks.html));

    const targetLinks = filterWebLinks(webLinks, peopleList);

    const uniqueLinks = getUniqueLinks(targetLinks, "href");

    await updatePeopleTable(uniqueLinks, professionId);
  }
};
