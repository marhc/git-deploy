const { unlinkSync } = require("fs");

module.exports = {
  filterWikiLinks(links, blackList) {
    return links.filter(link => {
      const parsed = parseInt(link);
      return parsed != parsed && blackList.join(",").indexOf(link) == -1;
    });
  },

  filterWebLinks(webLinks, writers) {
    return webLinks.filter(link => writers.join(",").indexOf(link.title) != -1);
  },

  getAttribs(linkList) {
    const attribList = [];

    for (let i = 0; i < linkList.length; i++) {
      const a = linkList[i];
      attribList.push(a.attribs);
    }

    return attribList;
  },

  getUniqueLinks(arr, comp) {
    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e])
      .map(e => arr[e]);

    return unique;
  },
  removeFile(filePath) {
    try {
      unlinkSync(filePath);
      console.log("The file has been removed:", filePath);
    } catch (error) {
      console.log("File not found:", filePath);
    } finally {
      console.log("A new file will be created...\n");
    }
  },
  pause(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
};
