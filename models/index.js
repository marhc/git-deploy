module.exports = {
  people: {
    link_title: "",
    name: "",
    bio_url: "",
    image_url: ""
  },
  people_info: {
    link_title: "",
    birth_date: "",
    death_date: ""
  },
  professions: {
    id: "",
    name: "",
    article_title: "",
    exclusions: ""
  },
  occupations: {
    profession_id: "",
    link_title: ""
  }
};
