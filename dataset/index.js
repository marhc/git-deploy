const path = require("path");

module.exports = {
  peopleFile: path.resolve(__dirname, "people.csv"),
  professionsFile: path.resolve(__dirname, "professions.csv"),
  occupationsFile: path.resolve(__dirname, "occupations.csv")
};
