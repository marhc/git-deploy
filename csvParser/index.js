const papa = require("papaparse");

const ObjectsToCsv = require("objects-to-csv");

const { readFileSync } = require("fs");

const parseCsvFile = textFileData => {
  const result = papa.parse(textFileData, {
    header: true,
    dynamicTyping: true
  });
  return result.data;
};

const importCsvFile = filePath => {
  const csvFile = readFileSync(filePath, "utf8");
  return parseCsvFile(csvFile);
};

const appendCsv = async (objArray, filePath) => {
  const csv = new ObjectsToCsv(objArray);

  // Save to file:
  await csv.toDisk(filePath, { append: true });

  // Return the CSV file as string:
  // return await csv.toString();
};

module.exports = {
  getTable(filePath, model) {
    try {
      return importCsvFile(filePath);
    } catch (error) {
      console.log("File not found:", filePath);
      console.log("A new file will be created...\n");
      return [model];
    }
  },

  addRecord(record, filePath) {
    appendCsv([record], filePath);
    return record;
  }
};
